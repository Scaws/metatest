<?php

namespace MetaTest\Tests\App;

use PHPUnit\Framework\TestCase;
use App\App;

class AppTest extends TestCase
{
    private $testInstance;

    public function setUp(): void
    {
        $this->testInstance = new App();
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(App::class, $this->testInstance);
    }
}