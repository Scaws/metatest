<?php

namespace MetaTest\Tests\Api;

use PHPUnit\Framework\TestCase;
use App\Api;

class ApiTest extends TestCase
{
    private $testInstance;

    public function setUp(): void
    {
        $this->testInstance = new Api('baseUrl.com' , ['someParam'=>'value']);
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(Api::class, $this->testInstance);
    }

}