<?php

namespace MetaTest\Tests\Database;

use PHPUnit\Framework\TestCase;
use App\DatabaseConnection;

class DatabaseTest extends TestCase
{
    private $testInstance;

    public function setUp() : void
    {
        $this->testInstance = new DatabaseConnection();
    }

    public function testInstanceOf()
    {
        $this->assertInstanceOf(DatabaseConnection::class, $this->testInstance);
    }
}