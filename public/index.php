<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MetaTest</title>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<div class="container">
    <input type="button" class="btn btn-dark api-call" value="Matrix"/>
    <input type="button" class="btn btn-dark api-call" value="Matrix Reloaded"/>
    <input type="button" class="btn btn-dark api-call" value="Matrix Revolutions"/>

    <div id="main-content"></div>
</div>

</body>
<script type="text/javascript" src="js/buttons.js"></script>
</html>

