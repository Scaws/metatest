<?php

require_once __DIR__ . '/../vendor/autoload.php';
require('../config.php');


if(isset($_POST['search'])){
    // $apiRequestHandler
    $apibaseurl = 'http://www.omdbapi.com/';
    $apiParams = ['s'=>$_POST['search'], 'apikey' => $config['apikey']];
    $db = new \App\DatabaseConnection($config['dbhost'], $config['dbport'],$config['dbname'], $config['dbuser'],$config['dbpass']);
    $api = new \App\Api($apibaseurl, $apiParams);
    $app = new \App\App();
    $results = $api->getRequest();
    if(isset($results->Search)){
        $app->presentList($api);
        $db->insertRecords($results->Search);
    }
}