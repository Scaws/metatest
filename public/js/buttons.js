console.log('click');
$(".api-call").click(function(e) {
    e.preventDefault();

    $('#main-content').html('<div class="spinner-border" role="status">\n' +
        '        <span class="sr-only">Loading...</span>\n' +
        '    </div>');

    $.ajax({
        type: "POST",
        url: "ajaxHandler.php",
        data: {
            search: $(this).val(),
        },
        success: function(result) {
            $('#main-content').html(result);
        },
        error: function(result) {
            console.log(result);
            console.log('nope');
        }
    });
});
