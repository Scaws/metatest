<?php
/**
 * Created by PhpStorm.
 * User: scaws
 * Date: 17/07/19
 * Time: 12:15
 */

namespace App;

class Api
{
    private $baseurl;
    private $params;
    private $results_raw;
    private $results_obj;

    /**
     * Api constructor.
     * @param String $baseurl
     * @param array $params
     */
    public function __construct(String $baseurl, Array $params)
    {
        $this->baseurl = $baseurl;
        $this->params = isset($params) ? $params : '';
    }

    /**
     * @return mixed|string
     */
    public function getRequest()
    {
        $url = $this->baseurl.'?'.http_build_query($this->params);
        $this->results_raw = file_get_contents($url);
        $this->results_obj = !empty($this->results_raw) ? json_decode($this->results_raw) : '';
        return $this->results_obj;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results_obj;
    }
}