<?php

namespace App;

class App
{
    private $results;

    /**
     * @param Api $api
     */
    public function presentList(Api $api)
    {
        if(!empty($api->getResults())){
            $this->results = $api->getResults();
            $html = '<div class="record-list container">';
            if(isset($this->results->Search)){
                foreach($this->results->Search as $result){
                    $html .= $this->buildListRow($result);
                }
            }
            $html .= '</div>';
            echo $html;
        }
    }

    /**
     * @param Object $item
     * @return string
     */
    public function buildListRow(Object $item)
    {
        $posterFallback = 'https://dummyimage.com/300X425/666/fff.png&text=No+Image';
        $poster = !empty($item->Poster) && $item->Poster != 'N/A' ? $item->Poster : $posterFallback;

        $html = '<div class="card record">';
        $html .= '<div class="record-image"><img src="'.$poster.'"/></div>';
        $html .= '<h4 class="record-title">'.$item->Title.'</h4>';
        $html .= '<p class="record-year">Year : '.$item->Year.'</p>';
        $html .= '<p class="record-type">'.$item->Type.'</p>';
        $html .= '<a href="https://www.imdb.com/title/'.$item->imdbID.'" target="_blank" class="record-id btn btn-secondary" >Imdb Link</a>';
        $html .= '</div>';
        return $html;
    }
}