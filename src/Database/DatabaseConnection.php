<?php

namespace App;

class DatabaseConnection
{
    private $host;
    private $port;
    private $databaseName;
    private $user;
    private $password;
    private $connection;

    /**
     * DatabaseConnection constructor.
     * @param string $host
     * @param int $port
     * @param string $databaseName
     * @param string $user
     * @param string $password
     */
    public function __construct($host='', $port=0, $databaseName='' , $user='', $password='')
    {
        $this->host = $host;
        $this->port = $port;
        $this->databaseName = $databaseName;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Used to open database connection
     */
    private function connect()
    {
        $this->connection = new \mysqli($this->host, $this->user, $this->password, $this->databaseName, $this->port);
    }

    /**
     * Used to close datebase connection
     */
    private function close()
    {
        if(isset($this->connection)){
            $this->connection->close();
        }
    }

    /**
     * @param array $records
     */
    public function insertRecords(Array $records)
    {
        $this->connect();
        foreach ($records as $record) {
            $imdbId = isset($record->imdbID) ? $record->imdbID : '';
            $title = isset($record->Title) ? $record->Title : '';
            $year = isset($record->Year) ? $record->Year : '';
            $type = isset($record->Type) ? $record->Type : '';
            $poster = isset($record->Poster) ? $record->Poster : '';

            $recordQuery = 'INSERT IGNORE into records(imdbId,title,year,type) VALUES(?,?,?,?)';
            $prepareRecordQuery = $this->connection->prepare($recordQuery);
            $prepareRecordQuery->bind_param('ssss',$imdbId,$title,$year,$type);

            $poseterQuery = 'INSERT IGNORE into posters(imdbId,posterUrl) VALUES(?,?)';
            $preparePosterQuery = $this->connection->prepare($poseterQuery);
            $preparePosterQuery->bind_param('ss',$imdbId , $poster);

            if (!empty($imdbId) && !empty($title)) {
                $prepareRecordQuery->execute();
                if(!empty($imdbId) && !empty($poster) && $poster != 'N/A'){
                    $preparePosterQuery->execute();
                }
            }
        }
        $this->close();
    }

}