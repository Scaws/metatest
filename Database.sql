CREATE DATABASE metaTest;

USE metaTest;

CREATE TABLE records(
   imdbId VARCHAR(30),
   title VARCHAR(255),
   year VARCHAR(6),
   type VARCHAR(30),
   PRIMARY KEY( imdbId )
);

CREATE TABLE posters(
   imdbId VARCHAR(30),
   posterUrl VARCHAR(255),
   PRIMARY KEY( imdbId )
);